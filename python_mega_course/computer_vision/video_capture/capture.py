#!/usr/bin/env python3

"""Capture my video cameras motions"""

import time
import cv2

#video = cv2.VideoCapture(0, cv2.CAP_DSHOW) #movie.mp4, if you have video in your computer
video = cv2.VideoCapture(0)
captured_frames = 0
while True:
    captured_frames = captured_frames + 1

    check, frame = video.read()

    print(check)
    print(frame)

    #time.sleep(3)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow("captured", gray)
    key = cv2.waitKey(1)

    if key == ord("q"):
        break

print(captured_frames)
video.release()
cv2.destroyAllWindows()
