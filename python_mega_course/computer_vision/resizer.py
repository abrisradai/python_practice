#!/usr/bin/env python3

import cv2
import os

cwd = os.getcwd()
#instead of this probably should use glob package, I should look after it
pics = os.listdir(cwd + "\\img_to_resize")

save=False

for pic in pics:
    img = cv2.imread(cwd + "\\img_to_resize\\" + pic, 1)
    resized_pic = cv2.resize(img,(100,100))
    cv2.imshow(pic,resized_pic)
    cv2.waitKey(0)
    if save:
        os.mkdir(cwd + "\\resized_img")
        cv2.imwrite(cwd + "\\resized_img\\resized_" + pic,resized_pic)
    cv2.destroyAllWindows()

