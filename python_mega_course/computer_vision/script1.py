#!/usr/bin/env python3

import cv2
import os

cwd = os.getcwd() #this makes the pest absolute, until they are in the same directory

img=cv2.imread(cwd + "\\" +"galaxy.jpg",0)

print(type(img))
print(img)
shape = img.shape
print(shape)
print(img.ndim)

#                           w  ,h
resized_img=cv2.resize(img,(int(shape[1]/2),int(shape[0]/2)))
cv2.imwrite("galaxy_resized.jpg",resized_img)
cv2.imshow("Galaxy",resized_img)
cv2.waitKey(0)
cv2.destroyAllWindows()