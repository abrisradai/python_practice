#!/usr/bin/env python3

class Account:
    def __init__(self, filePath):
        self.filePath = filePath
        with open(filePath, "r") as f:
            self.balance=int(f.read())

    def withdraw(self, amount):
        self.balance = self.balance - amount

    def deposit(self, amount):
        self.balance = self.balance + amount

    def commit(self):
        with open(self.filePath, "w") as f:
            f.write(str(self.balance))

class Checking(Account):
    """This Class generate checking account object"""

    type="checking"

    def __init__(self, filePath, fee):
        self.fee = fee
        Account.__init__(self, filePath)

    def transfer(self, amount):
        self.balance = self.balance - amount - self.fee

checking = Checking("account//balance.txt", 1)
checking.transfer(100)
print(checking.balance)
print(checking.__doc__)